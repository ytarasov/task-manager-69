package ru.t1.ytarasov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.service.IUserDtoService;
import ru.t1.ytarasov.tm.dto.model.CustomUser;
import ru.t1.ytarasov.tm.dto.model.RoleDto;
import ru.t1.ytarasov.tm.dto.model.UserDto;
import ru.t1.ytarasov.tm.enumirated.RoleType;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
public class UserDetailsServiceBean implements UserDetailsService {

    @NotNull
    @Autowired
    private IUserDtoService userDtoService;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    @SneakyThrows
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final UserDto user = userDtoService.findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        final List<RoleDto> userRoles = user.getRoles();
        final List<String> roles = new ArrayList<>();
        userRoles.forEach(role -> roles.add(role.getRoleType().name()));
        return new CustomUser(User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    @PostConstruct
    public void initUsers() {
        createUser("admin", "admin", RoleType.ADMINISTRATOR);
        createUser("test", "test", RoleType.USER);
    }

    @SneakyThrows
    public boolean checkUser(@Nullable final String login,
                             @Nullable final String password,
                             @Nullable final RoleType roleType
    ) {
        return userDtoService.existsByLogin(login);
    }

    @SneakyThrows
    public void createUser(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final RoleType roleType
    ) {
        if (checkUser(login, password, roleType)) return;
        userDtoService.create(login, password, roleType);
    }
}
