package ru.t1.ytarasov.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.ytarasov.tm.api.endpoint.AuthEndpoint;
import ru.t1.ytarasov.tm.api.service.IUserDtoService;
import ru.t1.ytarasov.tm.dto.model.Result;
import ru.t1.ytarasov.tm.dto.model.UserDto;

import javax.annotation.Resource;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/auth")
@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.AuthEndpoint")
public class AuthEndpointImpl implements AuthEndpoint {

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserDtoService userDtoService;

    @Override
    @WebMethod
    @PostMapping("/login")
    public Result login(
            @NotNull @WebParam(name = "login") final String login,
            @NotNull @WebParam(name = "password") String password
    ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token =
                    new UsernamePasswordAuthenticationToken(login, password);
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            return new Result(authentication.isAuthenticated());
        } catch (@NotNull final Exception e) {
            return new Result(e);
        }
    }

    @Override
    @WebMethod
    @SneakyThrows
    @PostMapping("/profile")
    public UserDto profile() {
        @NotNull final SecurityContext securityContext = SecurityContextHolder.getContext();
        @NotNull final Authentication authentication = securityContext.getAuthentication();
        @NotNull final String login = authentication.getName();
        return userDtoService.findByLogin(login);
    }

    @Override
    public Result logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
        return new Result();
    }

}
