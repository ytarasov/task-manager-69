package ru.t1.ytarasov.tm.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.service.ProjectService;
import ru.t1.ytarasov.tm.util.UserUtil;

import java.util.Collection;

@Controller
public class ProjectsController {

    @Autowired
    private ProjectService projectService;

    @SneakyThrows
    public Collection<ProjectDto> getProjects() {
        return projectService.findByUserId(UserUtil.getUserId());
    }

    @SneakyThrows
    @GetMapping("/projects")
    public ModelAndView index() {
        return new ModelAndView("projects", "projects", getProjects());
    }

}
