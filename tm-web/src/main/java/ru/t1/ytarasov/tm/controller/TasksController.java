package ru.t1.ytarasov.tm.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ytarasov.tm.service.ProjectService;
import ru.t1.ytarasov.tm.service.TaskService;
import ru.t1.ytarasov.tm.util.UserUtil;

@Controller
public class TasksController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @SneakyThrows
    @GetMapping("/tasks")
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("tasks");
        modelAndView.addObject("tasks", taskService.findByUserId(UserUtil.getUserId()));
        modelAndView.addObject("projectService", projectService);
        return modelAndView;
    }

}
