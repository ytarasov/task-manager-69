package ru.t1.ytarasov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;

import java.util.List;

@Repository
public interface IProjectDtoRepository extends JpaRepository<ProjectDto, String> {

    List<ProjectDto> findByUserId(@NotNull final String userId);

    ProjectDto findByUserIdAndId(@NotNull final String userId, @NotNull final String id);

    void deleteByUserId(@NotNull final String userId) throws Exception;

    void deleteByUserIdAndId(@NotNull final String userId, @NotNull final String id);

}
