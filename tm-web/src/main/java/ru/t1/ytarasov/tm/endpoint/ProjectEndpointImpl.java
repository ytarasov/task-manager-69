package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.api.endpoint.ProjectEndpoint;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.ProjectEndpoint")
public class ProjectEndpointImpl implements ProjectEndpoint {

    @NotNull
    @Autowired
    private ProjectService projectService;

    @Override
    @WebMethod
    @PutMapping("/createWithUserId")
    public void create() {
        projectService.create();
    }

    @Override
    @WebMethod
    @PostMapping("/saveWithUserId")
    public void save(
            @NotNull
            @WebParam(name = "project")
            @RequestBody
            final
            ProjectDto project
    ) {
        projectService.save(project);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<ProjectDto> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public ProjectDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return projectService.count();
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        return projectService.existsById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "project")
            @RequestBody
            final
            ProjectDto project) {
        projectService.delete(project);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        projectService.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "projects")
            @RequestBody
            final
            List<ProjectDto> projects) {
        projectService.deleteAll(projects);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear();
    }

}
