package ru.t1.ytarasov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/project")
public interface ProjectEndpoint {

    @WebMethod
    @PutMapping("/createWithUserId")
    void create();

    @WebMethod
    @PostMapping("/saveWithUserId")
    void save(
            @WebParam(name = "project")
            @RequestBody ProjectDto project
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<ProjectDto> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    ProjectDto findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project")
            @RequestBody ProjectDto project);

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "projects")
            @RequestBody List<ProjectDto> projects
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

}
