package ru.t1.ytarasov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.ytarasov.tm.dto.model.Result;
import ru.t1.ytarasov.tm.dto.model.UserDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface AuthEndpoint {

    @WebMethod
    @PostMapping("/login")
    Result login(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password
    );

    @WebMethod
    @GetMapping("/profile")
    UserDto profile();

    @WebMethod
    @PostMapping("/logout")
    Result logout();

}
