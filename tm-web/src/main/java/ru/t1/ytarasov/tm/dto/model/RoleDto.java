package ru.t1.ytarasov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.enumirated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_roles")
public class RoleDto {

    @Id
    private String id = UUID.randomUUID().toString();

    @Column(name = "role_type")
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @ManyToOne
    private UserDto user;

    public RoleDto(@NotNull final RoleType roleType) {
        this.roleType = roleType;
    }

}
