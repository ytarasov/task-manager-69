package ru.t1.ytarasov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.dto.model.UserDto;
import ru.t1.ytarasov.tm.enumirated.RoleType;

public interface IUserDtoService {

    @Transactional
    void create(@Nullable final String login, @Nullable final String password, @Nullable final RoleType roleType) throws Exception;

    @Transactional
    void save(@Nullable final UserDto user) throws Exception;

    UserDto findByLogin(@Nullable final String login) throws Exception;

    boolean existsByLogin(@Nullable final String login) throws Exception;

}
