package ru.t1.ytarasov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.api.endpoint.TaskEndpoint;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.service.TaskService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/endpoint/tasks")
@WebService(endpointInterface = "ru.t1.ytarasov.tm.api.endpoint.TaskEndpoint")
public class TaskEndpointImpl implements TaskEndpoint {

    @NotNull
    @Autowired
    private TaskService taskService;

    @Override
    @WebMethod
    @PutMapping("/createWithUserId")
    public void create() {
        taskService.create();
    }

    @Override
    @WebMethod
    @PostMapping("/saveWithUserId")
    public void save(
            @NotNull
            @WebParam(name = "task")
            @RequestBody
            final
            TaskDto task
    ) {
        taskService.save(task);
    }

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public Collection<TaskDto> findAll() {
        return taskService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/findById/{id}")
    public TaskDto findById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        return taskService.findById(id);
    }

    @Override
    @WebMethod
    @GetMapping("/count")
    public long count() {
        return taskService.count();
    }

    @Override
    @WebMethod
    @GetMapping("/existsById/{id}")
    public boolean existsById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        return taskService.existsById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/delete")
    public void delete(
            @NotNull
            @WebParam(name = "task")
            @RequestBody
            final
            TaskDto task
    ) {
        taskService.delete(task);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(
            @NotNull
            @WebParam(name = "id")
            @PathVariable("id")
            final
            String id
    ) {
        taskService.deleteById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/deleteAll")
    public void deleteAll(
            @NotNull
            @WebParam(name = "tasks")
            @RequestBody
            final
            List<TaskDto> tasks) {
        taskService.deleteAll(tasks);
    }

    @Override
    @WebMethod
    @DeleteMapping("/clear")
    public void clear() {
        taskService.clear();
    }

}