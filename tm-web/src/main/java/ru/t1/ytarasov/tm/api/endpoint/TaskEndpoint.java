package ru.t1.ytarasov.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.t1.ytarasov.tm.dto.model.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;
import java.util.List;

@WebService
@RequestMapping("/api/task")
public interface TaskEndpoint {

    @WebMethod
    @PutMapping("/createWithUserId")
    void create();

    @WebMethod
    @PostMapping("/saveWithUserId")
    void save(
            @WebParam(name = "task")
            @RequestBody TaskDto task
    );

    @WebMethod
    @GetMapping("/findAll")
    Collection<TaskDto> findAll();

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDto findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @GetMapping("/existsById/{id}")
    boolean existsById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "id")
            @RequestBody TaskDto task
    );

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "task")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "tasks")
            @RequestBody List<TaskDto> tasks
    );

    @WebMethod
    @DeleteMapping("/clear")
    void clear();

}
