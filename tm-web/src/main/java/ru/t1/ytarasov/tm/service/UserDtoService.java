package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.IUserDtoRepository;
import ru.t1.ytarasov.tm.api.service.IUserDtoService;
import ru.t1.ytarasov.tm.dto.model.RoleDto;
import ru.t1.ytarasov.tm.dto.model.UserDto;
import ru.t1.ytarasov.tm.enumirated.RoleType;
import ru.t1.ytarasov.tm.exception.field.LoginEmptyException;
import ru.t1.ytarasov.tm.exception.field.PasswordEmptyException;
import ru.t1.ytarasov.tm.exception.field.RoleEmptyException;
import ru.t1.ytarasov.tm.exception.user.ExistsLoginException;
import ru.t1.ytarasov.tm.exception.user.UserNotFoundException;

import java.util.Collections;

@Service
public class UserDtoService implements IUserDtoService {

    @NotNull
    @Autowired
    private IUserDtoRepository userDtoRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable RoleType roleType) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (userDtoRepository.existsByLogin(login)) throw new ExistsLoginException();
        if (roleType == null) throw new RoleEmptyException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final RoleDto role = new RoleDto(roleType);
        role.setUser(user);
        user.setRoles(Collections.singletonList(role));
        userDtoRepository.saveAndFlush(user);
    }

    @Override
    public void save(@Nullable final UserDto user) throws Exception {
        if (user == null) throw new UserNotFoundException();
        userDtoRepository.saveAndFlush(user);
    }

    @Override
    public UserDto findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userDtoRepository.findByLogin(login);
    }

    @Override
    public boolean existsByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userDtoRepository.existsByLogin(login);
    }

}
