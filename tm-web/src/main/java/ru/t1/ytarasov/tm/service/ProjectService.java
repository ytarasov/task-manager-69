package ru.t1.ytarasov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ytarasov.tm.api.repository.IProjectDtoRepository;
import ru.t1.ytarasov.tm.dto.model.ProjectDto;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;

import java.util.Collection;
import java.util.List;

@Service
public class ProjectService {

    @NotNull
    @Autowired
    private IProjectDtoRepository projectDtoRepository;

    public void save(@NotNull final ProjectDto project) {
        projectDtoRepository.saveAndFlush(project);
    }

    public void create() {
        @NotNull final ProjectDto project = new ProjectDto("New project " + System.currentTimeMillis(),
                "New project");
        projectDtoRepository.saveAndFlush(project);
    }

    public Collection<ProjectDto> findAll() {
        return projectDtoRepository.findAll();
    }

    public ProjectDto findById(@NotNull final String id) {
        return projectDtoRepository.findById(id).orElse(null);
    }

    public long count() {
        return projectDtoRepository.count();
    }

    public boolean existsById(@NotNull final String id) {
        return projectDtoRepository.existsById(id);
    }

    public void delete(@NotNull final ProjectDto project) {
        projectDtoRepository.delete(project);
    }

    public void deleteById(@NotNull final String id) {
        projectDtoRepository.deleteById(id);
    }

    public void deleteAll(@NotNull final List<ProjectDto> projects) {
        projectDtoRepository.deleteAll(projects);
    }

    public void clear() {
        projectDtoRepository.deleteAll();
    }

    public void createWithUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ProjectDto project = new ProjectDto("New project " + System.currentTimeMillis(),
                "New project");
        project.setUserId(userId);
        projectDtoRepository.saveAndFlush(project);
    }

    public void saveWithUserId(@Nullable final String userId, @Nullable final ProjectDto project) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ProjectNotFoundException();
        project.setUserId(userId);
        projectDtoRepository.saveAndFlush(project);
    }

    public List<ProjectDto> findByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return projectDtoRepository.findByUserId(userId);
    }

    public ProjectDto findByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return projectDtoRepository.findByUserIdAndId(userId, id);
    }

    public void deleteByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        projectDtoRepository.deleteByUserId(userId);
    }

    public void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        projectDtoRepository.deleteByUserIdAndId(userId, id);
    }

}
