package ru.t1.ytarasov.tm.controller;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.ytarasov.tm.enumirated.Status;
import ru.t1.ytarasov.tm.dto.model.TaskDto;
import ru.t1.ytarasov.tm.service.ProjectService;
import ru.t1.ytarasov.tm.service.TaskService;
import ru.t1.ytarasov.tm.util.UserUtil;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private ProjectService projectService;

    @SneakyThrows
    @PostMapping("/task/create")
    public String create() {
        taskService.createWithUserId(UserUtil.getUserId());
        return "redirect:/tasks";
    }

    @SneakyThrows
    @GetMapping("task/delete/{id}")
    public String delete(@PathVariable("id") String id) {
        taskService.deleteByUserIdAndId(UserUtil.getUserId(), id);
        return "redirect:/tasks";
    }

    @SneakyThrows
    @GetMapping("/task/edit/{id}")
    public ModelAndView edit(@PathVariable("id") String id) {
        final TaskDto task = taskService.findByUserIdAndId(UserUtil.getUserId(), id);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projectService.findByUserId(UserUtil.getUserId()));
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

    @SneakyThrows
    @PostMapping("/task/edit/{id}")
    public String edit(@ModelAttribute TaskDto task, BindingResult result) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.saveWithUserId(UserUtil.getUserId(), task);
        return "redirect:/tasks";
    }

}
